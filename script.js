
"use strict";
/// === Check  the amount ====================================================//
const calculateSum = () =>{
    const submitButton = document.querySelector('.btn');
    const result = document.querySelector('.result');
    let inputNumbrs = [];


    submitButton.addEventListener('click', () => {
        //получаем массив
        let inputArray = document.querySelector(".number_array").value;
        let inputNumbrs = inputArray.split(',');

        let k = document.querySelector(".number_sum").value;
        let x ;
        inputNumbrs.forEach((elment) =>{
            inputNumbrs.forEach((elment2) =>{
               if(elment !== elment2){
                   if((parseInt(elment)+parseInt(elment2)) == k){
                       x = true;
                   }
               }
            });
        });

       if(x){
           result.innerHTML = 'True';
       }else {
           result.innerHTML = 'False';
       }
    });
   };

calculateSum();

//=======  longest subarray ==========================================================//
const subArray = () =>{
    const submitButton = document.querySelector('.btn_2');
    const result = document.querySelector('.result_2');
    const subArrayValue = document.querySelector('.subarray');
    let inputValue = [];


    submitButton.addEventListener('click', () => {
        //получаем массив
        let inputArray = document.querySelector(".array_2").value;
        let inputValue = inputArray.split(',');

        let suvArray = inputValue.filter((e, i, a) => a.indexOf(e) === i);
        subArrayValue.innerHTML = suvArray;
        result.innerHTML = suvArray.length;
    });
};

subArray();

//====  greatest common denominator =======================================//
const gcd = () =>{
    const submitButton = document.querySelector('.btn_3');
    const result = document.querySelector('.result_3');
    let inputValue = [];


    submitButton.addEventListener('click', () => {
        //получаем массив
        let inputArray = document.querySelector(".array_3").value;
        let inputValue = inputArray.split(',');
        let maxDenominator = 0;

        const gCD = (a,b) =>{
            if ( ! b) {
                return a;
            }
            return gCD(b, a % b);
        }

        inputValue.forEach((a, i ) => {
            if(inputValue[i+1] !== undefined){
                let maxVal = gCD(a, inputValue[i+1]);
                if (maxVal > maxDenominator) {
                     maxDenominator = maxVal;
                }
            }
        });

        result.innerHTML = maxDenominator;

    });
};

gcd();